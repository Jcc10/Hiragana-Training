/**
 * @Author: Joshua Cline <jcc10>
 * @Date:   2018-08-31T11:49:07-07:00
 * @Email:  wwjdtd@gmail.com
 * @Last modified by:   jcc10
 * @Last modified time: 2018-08-31T18:20:20-07:00
 */
/* esversion: 6 */
import {WriteingGame} from './writeingGame.js';
import {ReadingGame} from './readingGame.js';
import {Modal} from './modal.js';
import {tk} from './toolkit/toolkit.js';
import {HiraganaList} from './HiraganaList.js';
import {HiraganaBank} from './HiraganaBank.js';

var gameMode = "init";
var oldGameMode = "init";

var game = new WriteingGame();

var settingsModal = new Modal('#settings')
settingsModal.attachShow("#OpenSettings");
var hintModal = new Modal('#hintTable')
hintModal.attachShow("#OpenHintTable");
var HiraganaHintList = new HiraganaList('#hintList');
HiraganaHintList.treatTable();
var HiraganaBankTable = new HiraganaBank('#kanaBank');
HiraganaBankTable.onRebuild = function(){
  game.mode = game.modes.RELOAD;
  game.generateSymbolBank(HiraganaBankTable.bank);
  game.switcher();
};
HiraganaBankTable.treatTable();

function switchGame() {
  tk("#mode input").doEach(function(element, index) {
    if (element.checked) {
      gameMode = element.value;
    }
  });
  if (gameMode !== oldGameMode) {
    game.removeSwitcher();
    if (gameMode == "r2h"){
      game = new WriteingGame();
    } else if (gameMode == "h2r"){
      game = new ReadingGame();
    }
    HiraganaBankTable.rebuildBank();
    game.generateSymbolBank(HiraganaBankTable.bank);
    game.switcher();
    oldGameMode = gameMode;
  }
}

tk("#mode").addListener('input', switchGame)

switchGame();
