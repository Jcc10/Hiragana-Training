/**
 * @Author: Joshua Cline <jcc10>
 * @Date:   2018-08-31T16:44:16-07:00
 * @Email:  wwjdtd@gmail.com
 * @Last modified by:   jcc10
 * @Last modified time: 2018-08-31T17:33:01-07:00
 */
 /* esversion: 6 */
import {tk} from './toolkit/toolkit.js';

export class Game{
  constructor(){
    this.hiriganaArray = [];
    this.maxHirigana = 0;
    this.symbols = {};
    this.prSet = [];
    this.modes = {
      INIT: 0,
      PROMPT: 1,
      RESPOND: 2,
      RELOAD: 3
    }
    this.mode = this.modes.INIT;
  }

  prompt(){
    console.log("UnSet");
  }

  response(){
    console.log("UnSet");
  }

  switcher() {
    if(this.mode == this.modes.INIT){
      this.mode = this.modes.PROMPT;
      this.prompt();
    } else if(this.mode == this.modes.RESPOND){
      this.mode = this.modes.PROMPT;
      this.prompt();
    } else if(this.mode == this.modes.PROMPT){
      this.mode = this.modes.RESPOND;
      this.response();
    } else {
      this.mode = this.modes.PROMPT;
      this.prompt();
    }
  }

  attatchSwitcher(){
    // This is bad code but I don't think it's going to be to much of a problem.
    // it fixes the lower section. "this." does not work in listeners.
    var gameSelf = this;
    tk("#button").addListener("click", function(e){
      gameSelf.switcher();
    })
  }

  removeSwitcher(){
    var gameSelf = this;
    tk("#button").removeListener("click", function(e){
      gameSelf.switcher();
    })
  }

  generateSet(len){
    let hiragana = "";
    let romaji = "";
    for (let c = 0; c < len; c++) {
        let tempS = this.hiriganaArray[Math.floor(Math.random() * this.maxHirigana)];
        hiragana += tempS[0];
        romaji += tempS[1];
    }
    return [hiragana, romaji];
  }

  generateSymbolBank(activeHiragana){
    let tempHiraganaArray = []
    let tempSymbols = this.symbols;
    activeHiragana.forEach(function(k, i){
      tempHiraganaArray.push(tempSymbols[k]);
    })
    this.maxHirigana = tempHiraganaArray.length;
    this.hiriganaArray = tempHiraganaArray;
  }
}
