/* esversion: 6 */
import {tk} from './toolkit/toolkit.js';

export class HiraganaList{
  constructor(CssID){
    this.baseCSSID = CssID;
    this.rowsCSSID = this.baseCSSID + " tr";
    this.hiraganaItemCSSID = this.rowsCSSID + " td:not(.rowStuff):not(.emptyCell)";
    this.rowSelectorsCSSID = this.rowsCSSID + " td.rowStuff";
    this.hiraganaItem = tk(this.hiraganaItemCSSID);
    this.rowSelectors = tk(this.rowSelectorsCSSID);
  }

  treatTable(){
    let selfHiraganalist = this;
    this.hiraganaItem.doEach(function(element, id){
      selfHiraganalist.prosessHiraganaItem(element);
    });
    this.rowSelectors.doEach(function(element, id){
      selfHiraganalist.prosessRowSelectors(element);
    })
  }

  prosessHiraganaItem(elementItem){
    let hiriganaRegEx = /(.)<br>(.{1,})/;
    this.hiraganaItemTreat(elementItem, hiriganaRegEx.exec(elementItem.innerHTML)[2]);
  }

  hiraganaItemTreat(element, character){
    let selfHiraganalist = this;
    element.dataset.status = "enabled";
    element.dataset.char = character;
    element.addEventListener("click", function(){
      if (element.dataset.status == "enabled") {
        selfHiraganalist.hiraganaDisable(element);
      } else {
        selfHiraganalist.hiraganaEnable(element);
      }
    });
  }

  hiraganaEnable(element){
    element.dataset.status = "enabled";
    element.style["background-color"] = null;
    element.style["color"] = null;
  }

  hiraganaDisable(element){
    element.dataset.status = "disabled";
    element.style["background-color"] = "red";
    element.style["color"] = "red";
  }

  prosessRowSelectors(elementItem){
    let selfHiraganalist = this;
    let myRow = tk("td:not(.rowStuff):not(.emptyCell)", elementItem.parentElement);
    tk(".selectAll", elementItem.parentElement).addListener('click', function(){
      selfHiraganalist.rowSelectAll(myRow);
      selfHiraganalist.toggleFinally();
    });
    tk(".selectNone", elementItem.parentElement).addListener('click', function(){
      selfHiraganalist.rowSelectNone(myRow);
      selfHiraganalist.toggleFinally();
    });
  }

  rowSelectAll(row){
    let selfHiraganalist = this;
    row.doEach(function(element, index){
      selfHiraganalist.hiraganaEnable(element);
    })
  }

  rowSelectNone(row){
    let selfHiraganalist = this;
    row.doEach(function(element, index){
      selfHiraganalist.hiraganaDisable(element);
    })
  }

  toggleFinally(){

  }
}
