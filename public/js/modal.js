/**
 * @Author: Joshua Cline <jcc10>
 * @Date:   2018-08-31T18:01:54-07:00
 * @Email:  wwjdtd@gmail.com
 * @Last modified by:   jcc10
 * @Last modified time: 2018-08-31T18:20:45-07:00
 */
 /* esversion: 6 */
 import {tk} from './toolkit/toolkit.js';

export class Modal {
    constructor(ModalCSSID, builtInClose){
      builtInClose = builtInClose || true;
      this.modal = tk(ModalCSSID);
      let closeBtnCSSID = ModalCSSID + " .closeModal";
      if (builtInClose) {
        this.closeBtn = tk(closeBtnCSSID);
        this.attachCloseBtn();
      }

    }

    attachShow(cssID){
      let modalSelf = this;
      tk(cssID).addListener("click", function(e){
        modalSelf.show();
      })
    }

    attachHide(cssID){
      let modalSelf = this;
      tk(cssID).addListener("click", function(e){
        modalSelf.hide();
      })
    }

    attachCloseBtn(){
      let modalSelf = this;
      this.closeBtn.addListener("click", function(e){
        modalSelf.hide();
      })
    }

    show(){
      this.modal.changeStyle("display", "block");
    }
    hide(){
      this.modal.changeStyle("display", "none");
    }
}
