/**
 * @Author: Joshua Cline <jcc10>
 * @Date:   2018-08-31T16:44:10-07:00
 * @Email:  wwjdtd@gmail.com
 * @Last modified by:   jcc10
 * @Last modified time: 2018-08-31T17:31:21-07:00
 */
/* esversion: 6 */
import {hiriganaSet} from './hirigana.js';
import {Game} from "./baseGame.js";
import {tk} from './toolkit/toolkit.js';

export class WriteingGame extends Game {
  constructor(Game) {
    super(Game);
    this.symbols = hiriganaSet;
    let activeHirigana = Object.keys(hiriganaSet);
    this.generateSymbolBank(activeHirigana);
    this.attatchSwitcher();
  }

  prompt() {
    this.prSet = this.generateSet(5);
    let romaji = this.prSet[1];
    tk("#instructions").replaceText("Please Write:");
    tk("#prompt").replaceText(romaji);
    tk("#instructionsCont").replaceText("In Hiragana.");
    tk("#button").replaceText("Done Writing.");
  }

  response() {
    let romaji = this.prSet[1];
    let hiragana = this.prSet[0];
    tk("#instructions").replaceText("Did you write:");
    tk("#prompt").replaceText(hiragana);
    tk("#instructionsCont").replaceText("The romaji was: " + romaji);
    tk("#button").replaceText("Give me the next set!");
  }

}
