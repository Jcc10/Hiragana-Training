/* esversion: 6 */
import {tk} from './toolkit/toolkit.js';
import {HiraganaList} from './HiraganaList.js';


export class HiraganaBank extends HiraganaList {
  constructor(HiraganaList) {
    super(HiraganaList);
    this.hiraganaItemCheckboxCSSID = this.hiraganaItemCSSID + " input";
    this.allowRebuild = true;
    this.bank = [];
    this.onRebuild = function(){};

    let selfHiraganalist = this;
    tk(this.hiraganaItemCSSID).addListener('change', function(){
      selfHiraganalist.rebuildBank();
    } );
  }

  hiraganaItemTreat(element, character){
    element.innerHTML = '<input type="checkbox" name="kanaBankItems" value="' + character + '" checked>' + element.innerHTML;
  }

  prosessRowSelectors(elementItem){
    let selfHiraganalist = this;
    let myRow = tk("td:not(.rowStuff):not(.emptyCell) input[type='checkbox']", elementItem.parentElement);
    tk(".selectAll", elementItem.parentElement).addListener('click', function(){
      selfHiraganalist.allowRebuild = false;
      selfHiraganalist.rowSelectAll(myRow);
      selfHiraganalist.toggleFinally();
    });
    tk(".selectNone", elementItem.parentElement).addListener('click', function(){
      selfHiraganalist.allowRebuild = false;
      selfHiraganalist.rowSelectNone(myRow);
      selfHiraganalist.toggleFinally();
    });
  }

  rowSelectAll(row){
    let selfHiraganalist = this;
    row.doEach(function(element, index){
      element.checked = true;
    })
  }

  rowSelectNone(row){
    let selfHiraganalist = this;
    row.doEach(function(element, index){
      element.checked = false;
    })
  }

  toggleFinally(){
    this.allowRebuild = true;
    this.rebuildBank();
  }

  rebuildBank(){
    if(this.allowRebuild){
      let tempBank = [];
      tk(this.hiraganaItemCheckboxCSSID).doEach(function(element, index) {
        if (element.checked) {
          tempBank.push(element.value);
        }
      });
      this.bank = tempBank;
      this.onRebuild();
    }
  }

}
