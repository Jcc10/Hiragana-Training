/* esversion: 6 */
import {hiriganaSet} from './hirigana.js';
import {Game} from "./baseGame.js";
import {tk} from './toolkit/toolkit.js';

export class ReadingGame extends Game {
  constructor(Game) {
    super(Game);
    this.symbols = hiriganaSet;
    let activeHirigana = Object.keys(hiriganaSet);
    this.generateSymbolBank(activeHirigana);
    this.attatchSwitcher();
  }

  prompt() {
    this.prSet = this.generateSet(5);
    let hiragana = this.prSet[0];
    tk("#instructions").replaceText("Please Write:");
    tk("#prompt").replaceText(hiragana);
    tk("#instructionsCont").replaceText("In Romaji.");
    tk("#button").replaceText("Done Writing.");
  }

  response() {
    let romaji = this.prSet[1];
    let hiragana = this.prSet[0];
    tk("#instructions").replaceText("Did you write:");
    tk("#prompt").replaceText(romaji);
    tk("#instructionsCont").replaceText("The Hiragana was: " + hiragana);
    tk("#button").replaceText("Give me the next set!");
  }

}
